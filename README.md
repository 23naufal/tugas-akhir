# Tugas Akhir

1. Tugas akhir yang merupakan database perpustakaan
2. REST API yang digunakan yaitu :
- GET, berfungsi untuk membaca data/resource dari REST server
- POST, berfungsi untuk membuat sebuah data/resource baru di REST server
- PUT, berfungsi untuk memperbaharui data/resource di REST server
- DELETE, berfungsi untuk menghapus data/resource dari REST serve
- OPTIONS, berfungsi untuk mendapatkan operasi yang disupport pada resource dari REST server.
3. Cara memakai :
- Mula-mula, peminjam buku memasukkan data diri
- Kemudian memasukkan data buku yang dipinjam
- Untuk pengelola akan mendapatkan data diri peminjam dan buku yang dipinjam
- Untuk pengelola lagi, mereka bisa menambah, mengubah atau menghapus beberapa data yang diperlukan
