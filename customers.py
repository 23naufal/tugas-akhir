CREATE TABLE customers (
    USERID int AUTO_INCREMENT PRIMARY KEY,
    USERNAME varchar(100) NOT NULL,
    NAMADEPAN varchar(100) NOT NULL,
    NAMABELAKANG varchar(100) NOT NULL,
    EMAIL varchar(100) NOT NULL,
    UNIQUE (USERNAME,EMAIL)
);

from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='23naufal',
                              user='root',
                              password='dedekgemes')
        except Exception as e:
            print(e)
    
    def showUsers(self):
        pass
    
    def showUserById(self, **params):
        pass
    
    def insertUser(self, **params):
        try:
            column = ', '.join(list(params['values'].keys()))
            values = tuple(list(params['values'].values()))
            crud_query = '''insert into customers ({0}) values {1};'''.format(column, values)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def updateUserById(self, **params):
        try:
            userid = params['userid']
            values = self.restructureParams(**params['values'])
            crud_query = '''update customers set {0} where userid = {1};'''.format(values, userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def deleteUserById(self, **params):
        try:
            userid = params['userid']
            crud_query = '''delete from customers where userid = {0};'''.format(userid)

            cursor = self.db.cursor()
            cursor.execute(crud_query)
        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()
        
    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
